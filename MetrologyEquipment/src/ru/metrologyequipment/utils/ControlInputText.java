package ru.metrologyequipment.utils;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

public abstract class ControlInputText {
	public static Document limitText(int countSymbols) {
		return new PlainDocument() {
			@Override
            public void insertString(int a, String s, AttributeSet atr) throws BadLocationException {
                if (s == null)
                    return;
                if ((getLength() + s.length()) <= countSymbols)
                    super.insertString(a, s, atr);
            }
		};
	}
}
