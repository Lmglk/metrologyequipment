package ru.metrologyequipment.tablemodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.table.AbstractTableModel;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import ru.metrologyequipment.instrument.Instrument;
import ru.metrologyequipment.utils.HibernateUtil;

public class InstrumentTableModel extends AbstractTableModel {
	private final int COLUMN_COUNT = 7;	
	
	private ArrayList<Instrument> instruments;
	private SessionFactory sessionFactory = null;
	private String nameFilter;
	private String conditionFilter;
	
	public InstrumentTableModel(){
		nameFilter = null;
		conditionFilter = null;
		instruments = new ArrayList<>();
		sessionFactory = HibernateUtil.getSessionFactory();
		updateModel();
	}
	
	@Override
	public String getColumnName(int columnIndex){
		switch (columnIndex){
		case 0: return "��������";
		case 1: return "�������� �����";
		case 2: return "����������� �����";
		case 3: return "���� ��������� ��������";
		case 4: return "������������� �� ������";
		case 5: return "���. ���������";
		case 6: return "����������";
		default: return null;
		}
	}
	
	@Override
	public int getColumnCount() {
		return COLUMN_COUNT;
	}

	@Override
	public int getRowCount() {
		return instruments.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return (instruments.get(rowIndex).asString())[columnIndex];
	}
	
	public Instrument getInstrument(int rowIndex){
		return instruments.get(rowIndex);
	}
	
	public String getNameFilter(){
		return nameFilter;
	}
	
	public String getConditionFiter(){
		return conditionFilter;
	}
	
	public void setFilter(String name, String condition){
		nameFilter = name;
		conditionFilter = condition;
		updateModel();
	}
	
	public boolean addData(Instrument instrument){	
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Instrument.class);
		criteria.add(Restrictions.eq("serial_num", instrument.getSerialNum()));
		boolean operation = (criteria.uniqueResult() == null);
		if (operation) {
			session.save(instrument);
			session.getTransaction().commit();
		}
		session.close();
		updateModel();
		return operation;
	}
	
	public void editData(Instrument instrument){	
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(instrument);
		session.getTransaction().commit();
		session.close();
		updateModel();
	}
	
	public void deleteData(int rowTable){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(instruments.get(rowTable));
		session.getTransaction().commit();
		session.close();
		updateModel();
	}
	
	private void updateModel(){
		instruments.clear();		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Instrument.class);
		if (nameFilter != null)
			criteria.add(Restrictions.eq("name", nameFilter));
		if (conditionFilter != null)
			criteria.add(Restrictions.eq("condition", conditionFilter));
		instruments = (ArrayList<Instrument>) criteria.list();
		session.getTransaction().commit();
		session.close();
		Collections.sort(instruments, new Comparator<Instrument>() {
			@Override
			public int compare(Instrument arg0, Instrument arg1) {
				return (arg0.getName().toUpperCase()).compareTo(arg1.getName().toUpperCase());
			}
		});
		fireTableDataChanged();
	}
	
	public void close(){
		if (sessionFactory != null)
			HibernateUtil.shutdown();
	}

}
