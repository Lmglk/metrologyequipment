package ru.metrologyequipment.instrument;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="instruments")
public class Instrument {	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="name", length=25, nullable=false)
	private String name;
	
	@Column(name="serial_num", nullable=false)
	private int serial_num;
	
	@Column(name="inventory_num", nullable=false)
	private int inventory_num;
	
	@Column(name="last_inspection", nullable=false)
	private String last_inspection;
	
	@Column(name="responsible", nullable = false)
	private String responsible;
	
	@Column(name="condition", nullable = false)
	private String condition;
	
	@Column(name="comment")
	private String comment;	
	
	
    public Instrument(){
	}
	
    public void setName(String name){
    	this.name = name;
    }
    
    public void setSerialNum(int serial_num){
    	this.serial_num = serial_num;
    }
    
    public void setInventoryNum(int inventory_num){
    	this.inventory_num = inventory_num;
    }
    
    public void setLastInspection(String last_inspection){
    	this.last_inspection = last_inspection;
    }
    
    public void setResponsible(String responsible){
    	this.responsible = responsible;
    }
    
    public void setCondition(String condition){
    	this.condition = condition;
    }
    
    public void setComment(String comment){
    	this.comment = comment;
    }
    
    public String getName(){
    	return name;
    }
    
    public int getSerialNum(){
    	return serial_num;
    }
    
    public int getInventoryNum(){
    	return inventory_num;
    }
    
    public String getLastInspection(){
    	return last_inspection;
    }
    
    public String getResponsible(){
    	return responsible;
    }
    
    public String getCondition(){
    	return condition;
    }
    
    public String getComment(){
    	return comment;
    }
	
	public String[] asString(){
		String[] temp = new String[7];
		temp[0] = name;
		temp[1] = Integer.toString(serial_num);
		temp[2] = Integer.toString(inventory_num);
		temp[3] = last_inspection;
		temp[4] = responsible;
		temp[5] = condition;
		temp[6] = comment;
		
		return temp;
	}
}
