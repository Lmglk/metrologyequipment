package ru.metrologyequipment.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ru.metrologyequipment.instrument.Instrument;
import ru.metrologyequipment.tablemodel.InstrumentTableModel;
import ru.metrologyequipment.utils.ControlInputText;

public class AddWindow {
	private JDialog dialog;
	
	private JLabel nameLabel;
	private JLabel serialLabel;
	private JLabel inventoryLabel;
	private JLabel lastInspectionLabel;
	private JLabel yearLabel;
	private JLabel monthLabel;
	private JLabel dayLabel;
	private JLabel responsibleLabel;
	private JLabel conditionLabel;
	private JLabel commentLabel;
	
	private JTextField nameField;
	private JTextField serialField;
	private JTextField inventoryField;
	private JTextField yearField;
	private JTextField monthField;
	private JTextField dayField;
	private JTextField responsibleField;
	private JTextField commentField;
	
	private JPanel dataPanel;
	private JComboBox<String> conditionComboBox;
	
	private JButton addButton;
	private JButton cancelButton;
	
	private InstrumentTableModel model;
	private Instrument instrument;
	private boolean flagNewInstrument;
	
	public AddWindow(JFrame parent, InstrumentTableModel model, Instrument instrument){
		dialog = new JDialog(parent, true);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setResizable(false);
		dialog.setLayout(new GridBagLayout());
		
		nameLabel = new JLabel("��������:");
		serialLabel = new JLabel("�������� �����:");
		inventoryLabel = new JLabel("����������� �����:");
		lastInspectionLabel = new JLabel("���� ��������� ��������:");
		yearLabel = new JLabel("���:");
		monthLabel = new JLabel("�����:");
		dayLabel = new JLabel("����:");			
		responsibleLabel = new JLabel("������������� �� ������:");	
		conditionLabel = new JLabel("���������:");
		commentLabel = new JLabel("����������:");		
		
		nameField = new JTextField();
		serialField = new JTextField();
		inventoryField = new JTextField();	
		yearField = new JTextField();
		monthField = new JTextField();
		dayField = new JTextField();
		responsibleField = new JTextField();
		commentField = new JTextField();
		
		conditionComboBox = new JComboBox<>();
		conditionComboBox.addItem("��������");
		conditionComboBox.addItem("�� ��������");
		conditionComboBox.addItem("��������������");
		
		addButton = new JButton();
		cancelButton = new JButton("������");
		
		nameField.setDocument(ControlInputText.limitText(25));
		yearField.setDocument(ControlInputText.limitText(4));
		monthField.setDocument(ControlInputText.limitText(2));
		dayField.setDocument(ControlInputText.limitText(2));
		
		dataPanel = new JPanel();
		dataPanel.setLayout(new BoxLayout(dataPanel, BoxLayout.X_AXIS));
		dataPanel.add(yearLabel);
		dataPanel.add(yearField);
		dataPanel.add(monthLabel);
		dataPanel.add(monthField);
		dataPanel.add(dayLabel);
		dataPanel.add(dayField);
		
		serialField.addKeyListener(new onlyDigitListener());
		inventoryField.addKeyListener(new onlyDigitListener());
		yearField.addKeyListener(new onlyDigitListener());
		monthField.addKeyListener(new onlyDigitListener());
		dayField.addKeyListener(new onlyDigitListener());	
		responsibleField.addKeyListener(new onlySymbolListener());
		addButton.addActionListener(new addButtonListener());
		cancelButton.addActionListener(e->close());
		
		this.model = model;
		if (instrument != null){
			this.instrument = instrument;
			String[] data = instrument.getLastInspection().split("-");
			dialog.setTitle("���������");
			nameField.setEditable(false);
			serialField.setEditable(false);
			addButton.setText("��������");
			nameField.setText(instrument.getName());
			serialField.setText(String.valueOf(instrument.getSerialNum()));
			inventoryField.setText(String.valueOf(instrument.getInventoryNum()));
			yearField.setText(data[0]);
			monthField.setText(data[1]);
			dayField.setText(data[2]);
			responsibleField.setText(instrument.getResponsible());
			conditionComboBox.setSelectedItem(instrument.getCondition());
			commentField.setText(instrument.getComment());
			flagNewInstrument = false;
		} else {
			addButton.setText("��������");
			dialog.setTitle("����������");
			flagNewInstrument = true;
		}
		
		dialog.add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(nameField, new GridBagConstraints(1, 0, 6, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(serialLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(serialField, new GridBagConstraints(1, 1, 6, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(inventoryLabel, new GridBagConstraints(0, 2, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(inventoryField, new GridBagConstraints(1, 2, 6, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(lastInspectionLabel, new GridBagConstraints(0, 3, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(dataPanel, new GridBagConstraints(0, 4, 2, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));

		dialog.add(responsibleLabel, new GridBagConstraints(0, 5, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(responsibleField, new GridBagConstraints(1, 5, 6, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(conditionLabel, new GridBagConstraints(0, 6, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(conditionComboBox, new GridBagConstraints(1, 6, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(commentLabel, new GridBagConstraints(0, 7, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(commentField, new GridBagConstraints(1, 7, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(addButton, new GridBagConstraints(0, 8, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(cancelButton, new GridBagConstraints(1, 8, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
	}
	
	private class addButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String name = nameField.getText();
			int serial = 0;
			if (!serialField.getText().equals(""))
				serial = Integer.parseInt(serialField.getText());
			int inventory = 0;
			if (!inventoryField.getText().equals(""))
				inventory = Integer.parseInt(inventoryField.getText());
			String year = yearField.getText();
			String month = monthField.getText();
			String day = dayField.getText();
			String responsible = responsibleField.getText();
			String condition = String.valueOf(conditionComboBox.getSelectedItem());
			String comment = commentField.getText();
			if (name.equals("") || year.equals("") || month.equals("") || day.equals("") || responsible.equals("") || (serial == 0) || (inventory == 0))
				JOptionPane.showMessageDialog(dialog, "������. ���� �� ������ ���� ������", "������", JOptionPane.ERROR_MESSAGE);
			else {
				if (instrument == null)
					instrument = new Instrument();
				instrument.setName(name);
				instrument.setSerialNum(serial);
				instrument.setInventoryNum(inventory);
				instrument.setLastInspection(year+"-"+month+"-"+day);
				instrument.setResponsible(responsible);
				instrument.setCondition(condition);
				instrument.setComment(comment);
				if (flagNewInstrument)
					if (model.addData(instrument))
						close();
					else
						JOptionPane.showMessageDialog(dialog, "������. ������ � ����� �������� ������� ��� ����������", "������", JOptionPane.ERROR_MESSAGE);
				else {
					model.editData(instrument);
					close();
				}
			}
		}		
	}
	
	private class onlySymbolListener extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent e) {
			char a = e.getKeyChar();
			if (!Character.isLetter(a) && (a != '.') && (a != ' '))
				e.consume();
		}
	}
	
	private class onlyDigitListener extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent e) {
			char a = e.getKeyChar();
			if (!Character.isDigit(a) && (a != '\b'))
				e.consume();
		}
	}
	
	public void close(){
		dialog.dispose();
	}
}
