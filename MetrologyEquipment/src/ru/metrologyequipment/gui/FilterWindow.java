package ru.metrologyequipment.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import ru.metrologyequipment.tablemodel.InstrumentTableModel;
import ru.metrologyequipment.utils.ControlInputText;


public class FilterWindow {
	private JDialog dialog;
	private JLabel nameLabel;
	private JLabel conditionLabel;
	private JTextField nameField;
	private JComboBox<String> conditionComboBox;
	private JButton okButton;
	private JButton resetButton;
	private JButton cancelButton;
	
	FilterWindow(JFrame parent, InstrumentTableModel model){
		dialog = new JDialog(parent, true);
		dialog.setTitle("������");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setResizable(false);
		dialog.setLayout(new GridBagLayout());
		
		nameLabel = new JLabel("��������:");
		nameField = new JTextField();
		conditionLabel = new JLabel("���������:");
		conditionComboBox = new JComboBox<>();
		conditionComboBox.addItem("<�������� ���������>");
		conditionComboBox.addItem("��������");
		conditionComboBox.addItem("�� ��������");
		conditionComboBox.addItem("��������������");
		okButton = new JButton("Ok");
		cancelButton = new JButton("������");
		resetButton = new JButton("�������� �������");
		
		nameField.setDocument(ControlInputText.limitText(25));
		
		okButton.addActionListener(e->{
			String name = null;
			String condition = null;
			if (!(nameField.getText().equals("")))
				name = nameField.getText();
			if (conditionComboBox.getSelectedIndex() != 0)
				condition = (String) conditionComboBox.getSelectedItem();
			System.out.println("NAME: " + name);
			System.out.println("CONDITION: " + condition);
			model.setFilter(name, condition);
			close();
		});
		resetButton.addActionListener(e->{
			nameField.setText("");
			conditionComboBox.setSelectedIndex(0);
			model.setFilter(null, null);
		});
		cancelButton.addActionListener(e->close());
		
		String name = model.getNameFilter();
		String condition = model.getConditionFiter();
		if (name != null)
			nameField.setText(name);
		if (condition != null)
			conditionComboBox.setSelectedItem(condition);
		
		dialog.add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(nameField, new GridBagConstraints(1, 0, 2, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(conditionLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(conditionComboBox, new GridBagConstraints(1, 1, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(resetButton, new GridBagConstraints(0, 2, 3, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(okButton, new GridBagConstraints(0, 3, 2, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.add(cancelButton, new GridBagConstraints(2, 3, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5), 0, 0));
		
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
	}
	
	public void close(){
		dialog.dispose();
	}
}