package ru.metrologyequipment.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;

import ru.metrologyequipment.tablemodel.InstrumentTableModel;

public class MainWindow {
	JFrame window;
	JMenuBar menuBar;
	JMenu fileMenu;
	JMenu editMenu;
	JMenu findMenu;
	JMenuItem exitMenuItem;
	JMenuItem addMenuItem;
	JMenuItem editMenuItem;
	JMenuItem deleteMenuItem;
	JMenuItem findMenuItem;
	
	JTable table;
	InstrumentTableModel model;
	JScrollPane scrollPane;
	JButton addButton;
	JButton filterButton;
	JButton editButton;
	JButton deleteButton;
	
	public MainWindow() {
		window = new JFrame();
		window.setTitle("���� ���������������� ������������");
		window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		window.addWindowListener(new windowListener());
		window.setLayout(new GridBagLayout());
		
		//Creating menu
		menuBar = new JMenuBar();
		fileMenu = new JMenu("����");
		editMenu = new JMenu("��������������");
		findMenu = new JMenu("�����");
		
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(findMenu);
		
		exitMenuItem = new JMenuItem("�����");
		addMenuItem = new JMenuItem("��������");
		editMenuItem = new JMenuItem("��������");
		deleteMenuItem = new JMenuItem("�������");
		findMenuItem = new JMenuItem("������");
		
		exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		addMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		editMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
		deleteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		findMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
		
		exitMenuItem.addActionListener(new exitWindowListener());		
		addMenuItem.addActionListener(new addDataListener());
		editMenuItem.addActionListener(new editDataListener());
		deleteMenuItem.addActionListener(new deleteDataListener());
		findMenuItem.addActionListener(new findDataListener());
		
		fileMenu.add(exitMenuItem);
		editMenu.add(addMenuItem);
		editMenu.add(editMenuItem);
		editMenu.add(deleteMenuItem);
		findMenu.add(findMenuItem);
		
		window.setJMenuBar(menuBar);
		//End creating menu
		
		model = new InstrumentTableModel();
		table = new JTable(model);
		table.getTableHeader().setReorderingAllowed(false);
		scrollPane = new JScrollPane(table);
		
		addButton = new JButton("��������");
		editButton = new JButton("��������");
		deleteButton = new JButton("�������");
		filterButton = new JButton("������");
		addButton.addActionListener(new addDataListener());
		editButton.addActionListener(new editDataListener());
		deleteButton.addActionListener(new deleteDataListener());
		filterButton.addActionListener(new findDataListener());
		
		window.add(scrollPane, new GridBagConstraints(0, 0, 4, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
        
        window.add(addButton, new GridBagConstraints(0, 1, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
		
        window.add(editButton, new GridBagConstraints(1, 1, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
        
        window.add(filterButton, new GridBagConstraints(2, 1, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
        
        window.add(deleteButton, new GridBagConstraints(3, 1, 1, 1, 1, 1,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
		
        window.setSize(800, 530);
        window.setResizable(false);
        window.setLocationRelativeTo(null);
		window.setVisible(true);
	}
	
	private class addDataListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			new AddWindow(window, model, null);
		}
	}
	
	private class editDataListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int row = table.getSelectedRow();
			if (row >= 0)
				new AddWindow(window, model, model.getInstrument(table.getSelectedRow()));
			else 
				JOptionPane.showMessageDialog(window, "������. �������� ������", "������", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private class deleteDataListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int row = table.getSelectedRow();
			if (row >= 0)
				model.deleteData(row);
			else 
				JOptionPane.showMessageDialog(window, "������. �������� ������", "������", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private class findDataListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			new FilterWindow(window, model);
		}
		
	}
	
	private class exitWindowListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Object[] options = { "��", "���" };
            int n = JOptionPane.showOptionDialog(window, "������� ����?", "�������������", 
            		JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            if (n == 0)
            	close();
		}
	}
	
	private class windowListener implements WindowListener {
		@Override
		public void windowClosing(WindowEvent e) {
			Object[] options = { "��", "���" };
            int n = JOptionPane.showOptionDialog(e.getWindow(), "������� ����?", "�������������", 
            		JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            if (n == 0)
            	close();
		}
		
		@Override
		public void windowActivated(WindowEvent e) {
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {
		}

		@Override
		public void windowOpened(WindowEvent e) {	
		}
		
	}
	
	private void close(){
		window.dispose();
		model.close();
	}
}
